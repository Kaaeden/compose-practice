package com.example.composepractice

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun ArticlePage()
{
    val image_ = painterResource(id = R.drawable.bg_compose_background)

    Box {
        Column {
            Image(painter = image_,
                contentDescription = null,
                modifier = Modifier
                    .fillMaxWidth()
            )
            Text("Jetpack Compose tutorial",
                fontSize = 20.sp,
                modifier = Modifier
                    .padding(all = 15.dp)
                    .wrapContentWidth(Alignment.Start)
            )
            Text("Jetpack Compose is a modern toolkit for building native Android UI. Compose simplifies and accelerates UI development on Android with less code, powerful tools, and intuitive Kotlin APIs.",
                fontSize = 15.sp,
                modifier = Modifier
                    .padding(start = 15.dp, end = 15.dp)
                    .wrapContentWidth(Alignment.CenterHorizontally)
            )
            Text("In this tutorial, you build a simple UI component with declarative functions. You call Compose functions to say what elements you want and the Compose compiler does the rest. Compose is built around Composable functions. These functions let you define your app\\'s UI programmatically because they let you describe how it should look and provide data dependencies, rather than focus on the process of the UI\\'s construction, such as initializing an element and then attaching it to a parent. To create a Composable function, you add the @Composable annotation to the function name.",
                fontSize = 15.sp,
                modifier = Modifier
                    .padding(all = 15.dp)
                    .wrapContentWidth(Alignment.CenterHorizontally)
            )

        }

    }

}