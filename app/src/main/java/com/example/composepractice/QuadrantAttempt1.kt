package com.example.composepractice

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun Quadrants()
{
    Box(){
        Column {
            Row {
                Box(
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .fillMaxHeight(1f)
                        .background(color = Color.Green)
                        .padding(all = 15.dp)
                ) {
                    Text(
                        "Text composable",
                        fontSize = 1.sp,
                        fontWeight = FontWeight.Bold,
                    )
                }
                Box(
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .fillMaxHeight(1f)
                        .background(color = Color.Yellow)
                        .padding(all = 15.dp)
                )
                Text("Image composable",fontSize = 1.sp,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier)

            }
            Row{
                Box(
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .fillMaxHeight(1f)
                        .background(color = Color.Blue)
                        .padding(all = 15.dp)
                ) {
                    Text(
                        "Row composable",
                        fontSize = 1.sp,
                        fontWeight = FontWeight.Bold,
                        modifier = Modifier
                    )

                }
                Box(
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .fillMaxHeight(1f)
                        .background(color = Color.LightGray)
                        .padding(all = 15.dp))
                Text(
                    "Column composable",
                    fontSize = 1.sp,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                )
            }
        }

    }

}